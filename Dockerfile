FROM matrixcommander/matrix-commander
ARG UID=10000
ENV USER=matrix-downloader

RUN dnf install -y --nodocs yt-dlp jq
RUN useradd --create-home -u $UID $USER
USER $USER
WORKDIR /home/$USER
COPY --chmod=555 run.sh /entrypoint.sh
COPY --chmod=555 register.sh /register.sh
ENTRYPOINT /entrypoint.sh
