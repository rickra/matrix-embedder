#!/bin/sh

/bin/python3 /app/matrix_commander/matrix-commander -s /data/read_store -c /data/read_credentials.json --encrypted --verify EMOJI --login PASSWORD --device matrix-downloader-read "$@"
/bin/python3 /app/matrix_commander/matrix-commander -s /data/write_store -c /data/write_credentials.json --encrypted --verify EMOJI --login PASSWORD --device matrix-downloader-write "$@"
