#!/bin/sh

# initial setup (needs to be done outside for verification)
# TODO do here without verification?
#matrix-commander --encrypted --verify EMOJI --login PASSWORD --device matrix-commander --homeserver https://matrix.org --user-login <username>

# prefer system installation to commands in docker (shellscript works outside the container as well)
MATRIX_COMMANDER="$(command -v matrix-commander || echo /bin/python3 /app/matrix_commander/matrix-commander)"
MATRIX_COMMANDER="$MATRIX_COMMANDER --encrypted"

READ_CREDENTIALS="--store /data/read_store/ --credentials /data/read_credentials.json"
WRITE_CREDENTIALS="--store /data/write_store/ --credentials /data/write_credentials.json"

# remember to escape multiline strings, they will fuck up with read
$MATRIX_COMMANDER $READ_CREDENTIALS --print-event-id --listen FOREVER --room-invites JOIN --output JSON "$@" | \
	jq --raw-output --unbuffered '.source | "\(.room_id) \(.sender) \(.content["m.relates_to"]? == null) \(.content.body | tojson)"' | \
	while read ROOM_ID SENDER IS_STANDALONE QUOTED_BODY; do
		echo "$SENDER :: $QUOTED_BODY"

		# skip messages that are not standalone, e.g. replies (which have links embedded in them)
		[ "$IS_STANDALONE" = "false" ] && continue

		echo "$QUOTED_BODY" | grep --only-matching 'https://\S*' | while read MATCH; do
			(
				set -xe
				FILENAME="$(yt-dlp --no-simulate --print filename "$MATCH" || echo "FAILED")"
				[ "$FILENAME" = "FAILED" ] && exit

				SITENAME="$(echo "$MATCH" | cut -d/ -f 3 | sed -e 's/^www\.//' -e 's/\.com$//')"
				$MATRIX_COMMANDER $WRITE_CREDENTIALS --debug --room "$ROOM_ID" --file "$FILENAME"
				rm "$FILENAME"
			) #&  # TODO download multiple in parallel
		done
done
