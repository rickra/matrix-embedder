# Matrix Embedder

script based on [matrix-commander](https://github.com/8go/matrix-commander), [jq](https://github.com/jqlang/jq) and [yt-dlp](https://github.com/yt-dlp/yt-dlp) to download videos from links and upload the files to a matrix chat room.
Im using podman and podman-compose, but docker should work just as well.

## Usage in compose

if you do not intend to run as your own uid, you should preface these commands with:
```sh
mkdir --mode=777 data
```

Now create the sessions and verify them both with another client:
```sh
podman-compose run --rm --entrypoint '/register.sh' downloader --homeserver "$HOMESERVER" --user-login "$USERLOGIN" --password "$PASSWORD" --room-default -
```

two sessions are required since matrix-commander cannot run two instance in parallel, see [this issue](https://github.com/8go/matrix-commander/issues/31)

And run it in compose:
```sh
podman-compose up -d
```

## Usage without containers

Install `matrix-commander`, `jq` and `yt-dlp`.

TODO: adapt run.sh such that it accepts locally stored sessions (needs multiple)
